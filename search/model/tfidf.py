from __future__ import division

import os

import jieba.posseg
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from io import open

root_path = os.path.dirname(os.path.abspath(__file__))


def get_data(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        contents = f.read()
        contents = contents.split("\n")

    return contents


def clean(contents, stop_words):
    pos = ['n', 'nz', 'v', 'vd', 'vn', 'l', 'a', 'd']
    raw_corpus = []
    join_corpus = []
    for content in contents:
        cut_words = jieba.posseg.cut(content)
        cut_words = [w.word for w in cut_words if w.flag in pos
                     and w.word not in stop_words]

        raw_corpus.append("".join(cut_words))
        join_corpus.append(" ".join(cut_words))

    return raw_corpus, join_corpus


def get_key_words(corpus, top, stop_words):
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(corpus)
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(X)
    weight = tfidf.toarray()
    word = vectorizer.get_feature_names()

    key_words = []
    for index in range(len(weight)):
        df = pd.DataFrame()
        # for i in range(len(word)):
        #     print(word[i] + "/" + str(weight[index][i]), end="\t")
        df['word'] = word
        df['weight'] = weight[index]
        df = df.sort_values(by="weight", ascending=False)

        keys = df['word'].tolist()
        keys = "".join(keys[:top * 2])

        key_words.append(keys)
        # print()

    key_word_return = []
    for i in range(len(key_words)):
        key = key_words[i]
        key = jieba.posseg.cut(key)
        item_return = []
        for item in key:
            if item.flag == 'n' and item.word not in stop_words \
                    and item.word in corpus[i]:
                item_return.append(item.word)
        key_word_return.append(" ".join(item_return[:top]))
    return key_word_return


def end_process(key_words, raw_data):
    for i in range(len(raw_data)):
        row = list(jieba.posseg.cut(raw_data[i]))
        key_dict = {}

        end = 4
        while end <= len(row):
            window = row[end - 4:end]
            for index in [3, 2, 1, 0]:
                if window[index].flag == 'a':
                    down = index - 1
                    while down >= 0 and window[down].flag != 'n':
                        down -= 1
                    if window[down].flag == 'n':
                        key_dict[window[down].word] = window[index].word

            end += 4

        keys_return = []
        keys = key_words[i].split(" ")
        for key, value in key_dict.items():
            for contain_key in keys:
                if key.find(contain_key) != -1:
                    keys_return.append(key + value)

        return keys_return


def key_extract():
    _stop_words = [w.strip() for w in open(os.path.normpath(os.path.join(root_path, 'data', 'stopWord.txt')), 'r',
                                           encoding='utf-8').readlines()]
    data = get_data(os.path.normpath(os.path.join(root_path, 'data', 'key_extract.txt')))
    raw_data, cut_data = clean(data, _stop_words)
    _key_words = get_key_words(cut_data, 6, _stop_words)
    keys = end_process(_key_words, raw_data)

    return keys

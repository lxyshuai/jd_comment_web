# coding=utf-8
from __future__ import division
import requests
import json
import time
from io import open

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0'}
cookies = "__jda=122270672.333023451.1540694318.1540739810.1540779694.6; unpl=V2_ZzNtbUVfEBx0WE5TKRtZDGICElQRVkMTdgoRA3sYWw1iUEddclRCFXwURlVnGlQUZwIZXkJcQRZFCHZXeBhYBmYBG1hyZ3MWdThOZHkbXgRkARdZQmdzEkU4dlVLGGwEV04QX0BWQBdwDEZkeilf; __jdv=122270672|kong|t_220520384_|tuiguang|69b80a96c3484018b01723ff10694bd1-p_1|1540779693995; __jdu=333023451; PCSYCityID=1601; shshshfp=59be919d1e295fd5a6e9848c0fbca365; shshshfpa=9853466f-8989-744f-3cd4-1b2fca2e3234-1540694336; shshshfpb=132f62f6054594e10ba3c37b69ba34b1617a52b19591451765bd521416; ipLoc-djd=1-72-4137-0; areaId=1; user-key=38133f60-9f69-4fb9-baf0-0d006a243f3a; cn=0; __jdb=122270672.5.333023451|6.1540779694; __jdc=122270672; shshshsID=e8e70bf1890488cafef8e60992945f6d_5_1540780258866; 3AB9D23F7A4B3C9B=R34FD46WYJYNL6MM7SVQPBA3RQ2QVLKAKPDCGQ3YJNVSPSNDGVZ3AXGS2DBPUMIBYCBWUNWBQKF3FPWGPO5L2GDZLY"
cookies = dict(map(lambda x: x.split('='), cookies.split(";")))

categories = {0: "", 1: "差评", 2: "中评", 3: "好评"}


def get_params_dict(params_str):
    params_dict = {}
    param_array = params_str.split('&')
    for item in param_array:
        key_value = item.split('=')
        params_dict[key_value[0]] = key_value[1]

    return params_dict


def get_next_url(url_str, params_dict):
    for item in params_dict.items():
        param = str(item[0]) + "=" + str(item[1]) + "&"
        url_str = url_str + param

    return url_str[:-1]


def get_last_position(str, subStr):
    last_position = -1
    while True:
        if str.find(subStr, last_position + 1) == -1:
            break
        last_position = str.find(subStr, last_position + 1)

    return last_position


def get_save_comments(url, jd_address, file_path):
    jd_url = jd_address[:jd_address.find('?')] + "?"
    jd_params = jd_address[jd_address.find('?') + 1:]
    jd_params = get_params_dict(jd_params)
    product_id = url.split('/')[-1].split('.')[0]
    jd_params['productId'] = product_id

    comment_type = [0]
    # print("start to fetch comments...")
    for num in comment_type:
        jd_params['page'] = 0
        jd_params['score'] = num

        save_path = file_path + "_" + categories[num] + ".txt"
        file = open(save_path, 'w', encoding='utf-8')

        while True:
            jd_next_url = get_next_url(jd_url, jd_params).replace('\n', '')
            # print("process next url: " + jd_next_url)
            # print("current page: " + str(jd_params['page']))

            response = requests.get(jd_next_url)
            response = response.text
            start = response.find("{")
            end = get_last_position(response, '}') + 1
            response = response[start:end]
            if not response:
                continue
            response = json.loads(response)
            # print(response)

            comments = response['comments']
            if len(comments) == 0:
                # print("end to process comments")
                break

            for comment in comments:
                content = comment['content']
                if content != u'此用户未填写评价内容':
                    file.write(comment['content'] + "\n\n")
                else:
                    # print("skip this comment: " + content)
                    pass

            jd_params['page'] = jd_params['page'] + 1
            # print("sleep for 1 seconds...\n")

        # print("end to fetch " + categories[num] + " comments...")
        file.close()

    # print("end to fetch comments...")

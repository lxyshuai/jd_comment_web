# coding=utf-8
from __future__ import division
import os
import jd_spider
from format_data import process_key_extract_data
from format_data import process_sentiment_analysis_data
from tfidf import key_extract
from svm import SVM
from train_model import train
from io import open

root_path = os.path.dirname(os.path.abspath(__file__))

def get_addresses(file_path):
    address_dict = {}
    file = open(file_path, 'r')
    for line in file:
        array = line.split(' ')
        if not array[0] in address_dict:
            address_dict[array[0]] = array[1]
    file.close()

    return address_dict


def process_single_url(_url):
    _address_path = os.path.normpath(os.path.join(root_path, 'data', 'address.txt'))
    addresses = get_addresses(_address_path)

    jd_address = addresses['jd']
    _save_path = os.path.normpath(os.path.join(root_path, 'data', 'jdComment'))
    _url = _url.strip()
    # print("process url: " + _url)
    jd_spider.get_save_comments(_url, jd_address, _save_path)


def get_data(url):
    # print(os.path.realpath(__file__))
    # print(os.path.split(os.path.realpath(__file__)))
    # print(os.getcwd())
    if not os.path.exists(os.path.normpath(os.path.join(root_path, 'data', 'svm_model.txt'))):
        train()

    process_single_url(url)
    process_sentiment_analysis_data()
    process_key_extract_data()

    with open(os.path.normpath(os.path.join(root_path, 'data', 'sentiment_analysis.txt')), 'r',
              encoding='utf-8') as file:
        data = [l.strip() for l in file]
    svm = SVM()
    predicts = svm.predict(data)
    pos, neg = 0, 0
    for p in predicts:
        if p == 1:
            pos += 1
        else:
            neg += 1

    right_count = pos
    wrong_count = neg
    keys = key_extract()

    data = {'count':
                [{'value': right_count, 'name': u'好评'},
                 {'value': wrong_count, 'name': u'差评'}],
            'key': keys}
    return data


if __name__ == '__main__':
    _url = "https://item.jd.com/598283.html"
    right, wrong, key = get_data(url=_url)

    # print("right: ", right)
    # print("wrong: ", wrong)
    # print("key: ", key)

# coding=utf-8
from __future__ import division

import os

import jieba
import re
from io import open

root_path = os.path.dirname(os.path.abspath(__file__))


def get_stop_words(stop_word_path):
    stop_word_file = open(stop_word_path, 'r', encoding='utf-8')
    stop_words = [line.strip() for line in stop_word_file.readlines()]
    stop_word_file.close()

    return stop_words


def clean(contents, stop_words):
    pattern = "[A-Za-z\d\s]"
    cut_contents = []
    for item in contents:
        words = jieba.cut(item)

        item_cuts = [re.sub(pattern, "", word) for word in words]
        item_cuts = [word for word in item_cuts if word not in stop_words
                     and not word.isspace()]
        if item_cuts not in cut_contents:
            cut_contents.append(item_cuts)

    return cut_contents


def process_sentiment_analysis_data():
    with open(os.path.normpath(os.path.join(root_path, 'data', 'jdComment_.txt')), 'r', encoding="utf-8") as file:
        contents = file.read()

    contents = contents.split("\n\n")
    _stop_words = get_stop_words(os.path.normpath(os.path.join(root_path, 'data', 'stop_word.txt')))
    contents = clean(contents, _stop_words)

    # print("comment len: " + str(len(contents)))

    with open(os.path.normpath(os.path.join(root_path, 'data', 'sentiment_analysis.txt')), 'w',
              encoding='utf-8') as file:
        for content in contents:
            if len(content) == 0:
                continue
            for i in range(len(content)):
                if i == len(content) - 1:
                    file.write(content[i] + "\n")
                else:
                    file.write(content[i] + "\t")


def process_key_extract_data():
    data_list = [os.path.normpath(os.path.join(root_path, 'data', 'jdComment_.txt'))]

    combine_data = []
    for path in data_list:
        with open(path, 'r', encoding="utf-8") as f:
            contents = f.read()
            contents = contents.replace("\n", u"。")
            combine_data.append(contents)

    for data in combine_data:
        with open(os.path.normpath(os.path.join(root_path, 'data', 'key_extract.txt')), 'w', encoding='utf-8') as f:
            f.write(data)

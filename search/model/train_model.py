from __future__ import division

import os

from corpus import Corpus
from svm import SVM
root_path = os.path.dirname(os.path.abspath(__file__))

class JdCorpus(Corpus):
    def __int__(self):
        Corpus.__init__(self, "")


def train():
    _train_num = 2000
    _test_num = 482
    _feature_num = 550
    _max_iter = 100
    _C = 55
    _jd_corpus = JdCorpus(os.path.normpath(os.path.join(root_path, 'data', 'train.txt')))
    svm = SVM()
    svm.init(_train_num, _test_num, _feature_num, _C, _max_iter, _jd_corpus)
    svm.train()

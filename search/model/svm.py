from __future__ import division
from feature_extraction import ChiSquare
from tools import get_accuracy
from classifiers import SVMClassifier
import pickle


class SVM:
    def __init__(self):
        self.train_num = None
        self.test_num = None
        self.feature_num = None
        self.max_iter = None
        self.classifier_num = None
        self.corpus = None
        self.parameters = None
        self.precisions = None
        self.train_data, self.train_labels = None, None
        self.test_data, self.test_labels = None, None
        self.best_words = None
        self.svm = SVMClassifier(None, None, None, None)

    def init(self, train_num, test_num, feature_num, C, max_iter, corpus):
        self.train_num = train_num
        self.test_num = test_num
        self.feature_num = feature_num
        self.max_iter = max_iter
        self.classifier_num = C
        self.corpus = corpus

        self.parameters = [train_num, test_num, feature_num]
        self.precisions = [[0, 0],  # bayes
                           [0, 0],  # maxent
                           [0, 0]]  # svm

        self.train_data, self.train_labels = corpus.get_train_corpus(train_num)
        # print("train_data len " + str(len(self.train_data)))
        # print("train_labels len " + str(len(self.train_labels)))
        self.test_data, self.test_labels = corpus.get_test_corpus(test_num)
        # print("test_data len " + str(len(self.test_data)))
        # print("test_labels len " + str(len(self.test_labels)))

        cs = ChiSquare(self.train_data, self.train_labels)
        self.best_words = cs.best_words(self.feature_num)

    def write(self, classify_labels, i=-1):
        results = get_accuracy(self.test_labels, classify_labels, self.parameters)
        if i >= 0:
            self.precisions[i][0] = results[10][1] / 100
            self.precisions[i][1] = results[7][1] / 100

    def train(self):
        # print("SVMClassifier")
        # print("---" * 45)
        # print("Train num = %s" % self.train_num)
        # print("Test num = %s" % self.test_num)
        # print("C = %s" % self.classifier_num)

        self.svm = SVMClassifier(self.train_data, self.train_labels, self.best_words, self.classifier_num)
        self.svm.train()

        classify_labels = []
        # print("SVMClassifier is testing ...")
        for data in self.test_data:
            classify_labels.append(self.svm.classify(data))
        # print("SVMClassifier tests over.")

        self.write(classify_labels, 2)
        self.svm.save_model()

    def predict(self, test_data):
        self.svm.load_model()
        predicts = []
        for data in test_data:
            predicts.append(self.svm.classify(data))

        return predicts

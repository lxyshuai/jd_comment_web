from __future__ import division

import os
import numpy as np
import pickle
from sklearn.svm import SVC

root_path = os.path.dirname(os.path.abspath(__file__))


class SVMClassifier:
    def __init__(self, train_data, train_labels, best_words, C):
        self.train_data = np.array(train_data)
        self.train_labels = np.array(train_labels)

        self.best_words = best_words
        self.clf = SVC(C=C)
        self.loaded = False

    def words2vector(self, all_data):
        vectors = []

        best_words_index = {}
        for i, word in enumerate(self.best_words):
            best_words_index[word] = i

        for data in all_data:
            vector = [0 for x in range(len(self.best_words))]
            for word in data:
                i = best_words_index.get(word)
                if i is not None:
                    vector[i] = vector[i] + 1
            vectors.append(vector)

        vectors = np.array(vectors)
        return vectors

    def __train(self, train_data, train_labels):
        # print("SVMClassifier is training ...... ")

        train_vectors = self.words2vector(train_data)

        self.clf.fit(train_vectors, np.array(train_labels))

        # print("SVMClassifier trains over!")

    def classify(self, data):
        vector = self.words2vector([data])

        prediction = self.clf.predict(vector)

        return prediction[0]

    def train(self):
        self.__train(self.train_data, self.train_labels)

    def load_model(self):
        if not self.loaded:
            self.clf = pickle.load(open(os.path.normpath(os.path.join(root_path, 'data', 'svm_model')), 'rb'))
            self.best_words = pickle.load(
                open(os.path.normpath(os.path.join(root_path, 'data', 'svm_best_words')), 'rb'))

    def save_model(self):
        pickle.dump(self.clf, open(os.path.normpath(os.path.join(root_path, 'data', 'svm_model')), 'wb'))
        pickle.dump(self.best_words, open(os.path.normpath(os.path.join(root_path, 'data', 'svm_best_words')), 'wb'))

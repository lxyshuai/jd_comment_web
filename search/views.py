# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from model.interface import get_data


# Create your views here.


def index(request):
    return render(request, 'search/index.html')


@csrf_exempt
def search(request):
    data = get_data(request.POST['url'])
    return JsonResponse(data, safe=False)
